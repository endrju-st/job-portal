import {Component, OnInit} from '@angular/core';
import {Developer} from "./../model/Developer";
import {DeveloperService} from "./../services/developer.service";
import {HttpErrorResponse} from "@angular/common/http";
import {ActivatedRoute} from "@angular/router";
import {switchMap} from "rxjs/operators";

@Component({
  selector: 'app-developer-detail',
  templateUrl: './developer-detail.component.html',
  styleUrls: ['./developer-detail.component.scss']
})
export class DeveloperDetailComponent implements OnInit {
  public developers: Developer[];
  public developer: Developer;
  public id: string;

  constructor(private developerService: DeveloperService,
              private router: ActivatedRoute) {
  }

  ngOnInit() {
    this.router.paramMap.subscribe(params=>this.id=params.get('id'))
    this.getDevelopersById(this.id);
  }

  public getDevelopersById(id): void {
    this.developerService.getDeveloperById(id).subscribe(
      (response: Developer) => {
        this.developer = response
      },
      (error: HttpErrorResponse) => {
        alert(error.message)
      }
    )
  }


}
