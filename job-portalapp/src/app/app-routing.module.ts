import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DeveloperDetailComponent } from './developer-detail/developer-detail.component';
import { DevelopersComponent } from './developers/developers.component';

const routes: Routes = [
  {
    path: 'developers',
    component: DevelopersComponent,
  },
  {
    path:'developers-details/:id',
    component: DeveloperDetailComponent,
  },
];

@NgModule({
  declarations: [],
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
