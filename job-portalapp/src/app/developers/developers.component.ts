import {Component, OnInit} from '@angular/core';
import {DeveloperService} from "../services/developer.service";
import {Developer} from "../model/Developer";
import {HttpErrorResponse} from "@angular/common/http";
import {ActivatedRoute} from "@angular/router";

@Component({
  selector: 'app-developers',
  templateUrl: './developers.component.html',
  styleUrls: ['./developers.component.scss']
})
export class DevelopersComponent implements OnInit {
  public developers: Developer[];

  constructor(private developerService: DeveloperService,
              private route: ActivatedRoute) {
  }

  developer = 'Leszek Woźnica'
  ngOnInit(): void {
    this.getDevelopers();
  }
  public getDevelopers(): void {
    this.developerService.getDevelopers().subscribe(
      (response: Developer[]) => {
        this.developers = response
      },
      (error: HttpErrorResponse) => {
        alert(error.message)
      }
    )
  }


}
