import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {Developer} from "../model/Developer";
import {environment} from "../../environments/environment";


@Injectable({
  providedIn: 'root'
})

export class DeveloperService {

  private apiServerUrl = environment.apiBaseUrl;

  constructor(private http: HttpClient) {
  }

  public getDevelopers(): Observable<Developer[]> {
    return this.http.get<Developer[]>(`${this.apiServerUrl}/devs/all`);
  }

  public getDeveloperById(id: number): Observable<Developer> {
    return this.http.get<Developer>(`${this.apiServerUrl}/devs/all/${id}`)
  }

  public addDeveloper(developer: Developer): Observable<Developer> {
    return this.http.post<Developer>(`${this.apiServerUrl}/devs/add`, developer);
  }

  public updateDeveloper(developer: Developer): Observable<Developer> {
    return this.http.put<Developer>(`${this.apiServerUrl}/devs/update`, developer);
  }

  public deleteDeveloper(developerId: number): Observable<void> {
    return this.http.delete<void>(`${this.apiServerUrl}/devs/delete/${developerId}`);
  }

}
