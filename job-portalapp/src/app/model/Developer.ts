export interface Developer {
  id:          number;
  username:    string;
  password:    string;
  email:       string;
  firstName:   string;
  lastName:    string;
  description: string;
  imageUrl:    string;
  skills:      null;
}
