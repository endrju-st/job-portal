import {Component, OnInit} from '@angular/core';
import {Developer} from "./model/Developer";
import {DeveloperService} from "./services/developer.service";
import {HttpErrorResponse} from "@angular/common/http";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit{
  public developers: Developer[];
  public developer: Developer;
  constructor(private developerService: DeveloperService) {
  }

  ngOnInit() {
    this.getDevelopers();
    this.getDevelopersById(1);
  }
  public getDevelopersById(id:number): void {
    this.developerService.getDeveloperById(id).subscribe(
      (response: Developer) => {
        this.developer = response
      },
      (error: HttpErrorResponse) => {
        alert(error.message)
      }
    )
  }


  public getDevelopers(): void {
    this.developerService.getDevelopers().subscribe(
      (response: Developer[]) => {
        this.developers = response
      },
      (error: HttpErrorResponse) => {
        alert(error.message)
      }
    )
  }
}
