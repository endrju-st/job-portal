package com.dieboldnixdorf.Jobportal.repository;

import com.dieboldnixdorf.Jobportal.model.Skill;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SkillRepository extends JpaRepository<Skill,Long> {
}
