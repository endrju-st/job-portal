package com.dieboldnixdorf.Jobportal.repository;

import com.dieboldnixdorf.Jobportal.model.Messages;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MessagesRepository extends JpaRepository<Messages,Long> {
}
