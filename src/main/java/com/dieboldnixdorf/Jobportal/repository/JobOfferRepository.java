package com.dieboldnixdorf.Jobportal.repository;

import com.dieboldnixdorf.Jobportal.model.JobOffer;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface JobOfferRepository extends JpaRepository<JobOffer,Long> {
    Optional<JobOffer> getJobOfferById(Long id);
}
