package com.dieboldnixdorf.Jobportal.repository;

import com.dieboldnixdorf.Jobportal.model.Developer;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface DeveloperRepository extends JpaRepository<Developer,Long> {
     void deleteDeveloperById(Long id);
     Optional<Developer> getDeveloperById(Long id);
     Optional<Developer> getDeveloperByEmail(String email);
}
