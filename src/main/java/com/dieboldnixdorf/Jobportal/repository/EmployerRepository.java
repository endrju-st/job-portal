package com.dieboldnixdorf.Jobportal.repository;

import com.dieboldnixdorf.Jobportal.model.Employer;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EmployerRepository extends JpaRepository<Employer,Long> {
    void getEmployerById(Long id);
}
