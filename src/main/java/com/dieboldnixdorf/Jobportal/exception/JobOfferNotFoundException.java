package com.dieboldnixdorf.Jobportal.exception;

public class JobOfferNotFoundException extends RuntimeException {

    public JobOfferNotFoundException(String message) {
        super(message);
    }
}
