package com.dieboldnixdorf.Jobportal.controllers;

import com.dieboldnixdorf.Jobportal.model.JobOffer;
import com.dieboldnixdorf.Jobportal.services.JobOfferService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@AllArgsConstructor
@RequestMapping("/api/joboffers/")

public class JobOfferController {
    private final JobOfferService jobOfferService;

    @GetMapping("all")
    public ResponseEntity<List<JobOffer>> getAllJobOffers(){
        List<JobOffer> allOffers = jobOfferService.getAllOffers();
        return new ResponseEntity<>(allOffers, HttpStatus.OK);
    }

    @GetMapping("all/{id}")
    public ResponseEntity<JobOffer> getOfferById(@PathVariable("id") Long id){
        JobOffer jobOffer = jobOfferService.findOfferById(id);
        return new ResponseEntity<>(jobOffer,HttpStatus.OK);
    }

    @GetMapping("add")
    public ResponseEntity<JobOffer> addJobOffer(@RequestBody JobOffer jobOffer){
        JobOffer newJobOffer = jobOfferService.addJobOffer(jobOffer);
        return new ResponseEntity<>(newJobOffer,HttpStatus.CREATED);
    }

    @PutMapping("update")
    public ResponseEntity<JobOffer> updateJobOffer(@RequestBody JobOffer jobOffer, Long id) {
        JobOffer updatedJobOffer = jobOfferService.updateJobOffer(jobOffer, id);
        return new ResponseEntity<>(updatedJobOffer,HttpStatus.OK);
    }
}
