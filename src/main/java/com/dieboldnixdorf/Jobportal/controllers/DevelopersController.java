package com.dieboldnixdorf.Jobportal.controllers;

import com.dieboldnixdorf.Jobportal.model.Developer;
import com.dieboldnixdorf.Jobportal.services.DevelopersService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.transaction.Transactional;
import java.util.List;

@RestController
@AllArgsConstructor
@RequestMapping("/api/devs/")
public class DevelopersController {

    private final DevelopersService developersService;

    @GetMapping("all")
    public ResponseEntity<List<Developer>> getAllDevs(){
        List<Developer> allDevs = developersService.getAllDevs();
        return new ResponseEntity<>(allDevs, HttpStatus.OK);
    }

    @GetMapping("all/{id}")
    public ResponseEntity<Developer> getDevById(@PathVariable("id") Long id){
        Developer developer = developersService.getDevById(id);
        return new ResponseEntity<>(developer, HttpStatus.OK);
    }

//    @GetMapping("all/{email}")
//    public ResponseEntity<Developer> getDevByEmail(@PathVariable("email") String email){
//        Developer developer = developersService.getDevByEmail(email);
//        return new ResponseEntity<>(developer,HttpStatus.OK);
//
//    }

    @PostMapping("/add")
    public ResponseEntity<Developer> addDeveloper(@RequestBody Developer developer) {
        Developer newDeveloper = developersService.addDeveloper(developer);
        return new ResponseEntity<>(newDeveloper, HttpStatus.CREATED);
    }

    @PutMapping("/update")
    public ResponseEntity<Developer> updateDeveloper(@RequestBody Developer developer, Long id) {
        Developer updatedDeveloper = developersService.updateDeveloperById(developer, id);
        return new ResponseEntity<>(updatedDeveloper, HttpStatus.OK);
    }

    @DeleteMapping("/delete/{id}")
    @Transactional
    public ResponseEntity<?> deleteDeveloper(@PathVariable("id") Long id) {
        developersService.deleteDeveloper(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }


}
