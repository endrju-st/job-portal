package com.dieboldnixdorf.Jobportal.controllers;

import com.dieboldnixdorf.Jobportal.model.Developer;
import com.dieboldnixdorf.Jobportal.model.Employer;
import com.dieboldnixdorf.Jobportal.services.EmployerService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.transaction.Transactional;
import java.util.List;

@RestController
@RequestMapping("/api/employers/")
@AllArgsConstructor
public class EmployerController {
    private final EmployerService employerService;

    @GetMapping("all")
    public ResponseEntity<List<Employer>> getAllEmployers(){
        List<Employer> allEmployers = employerService.getAllEmployers();
        return new ResponseEntity<>(allEmployers, HttpStatus.OK);
    }

    @GetMapping("all/{id}")
    public ResponseEntity<Employer> getEmployerById(@PathVariable("id") Long id){
        Employer employer = employerService.getEmployerById(id);
        return new ResponseEntity<>(employer, HttpStatus.OK);
    }

    @PostMapping("add")
    public ResponseEntity<Employer> addEmployer(@RequestBody Employer employer) {
        Employer newEmployer  = employerService.addEmployer(employer);
        return new ResponseEntity<>(newEmployer, HttpStatus.CREATED);
    }

    @PutMapping("update")
    public ResponseEntity<Employer> updateEmployer(@RequestBody Employer employer) {
        Employer updatedEmployer = employerService.addEmployer(employer);
        return new ResponseEntity<>(updatedEmployer, HttpStatus.OK);
    }

    @DeleteMapping("delete/{id}")
    @Transactional
    public ResponseEntity<?> deleteEmployer(@PathVariable("id") Long id) {
        employerService.deleteEmployer(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
