package com.dieboldnixdorf.Jobportal.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.List;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class JobOffer {

    @Id
    @GeneratedValue
    private Long id;
    private String title;
    private String description;
    private LocalDate createdDate;
    private LocalDate duration;
    private boolean active;
    @OneToOne
    private Employer employer;
    @OneToMany
    private List<Developer> developers;

}
