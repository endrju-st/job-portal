package com.dieboldnixdorf.Jobportal.configuration;

import com.dieboldnixdorf.Jobportal.model.Developer;
import com.dieboldnixdorf.Jobportal.model.Skill;
import com.dieboldnixdorf.Jobportal.repository.DeveloperRepository;
import com.dieboldnixdorf.Jobportal.repository.SkillRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.Arrays;

@Service
public class DataInit {

    private  final DeveloperRepository developerRepository;
    private final SkillRepository skillRepository;

    public DataInit(DeveloperRepository developerRepository, SkillRepository skillRepository) {
        this.developerRepository = developerRepository;
        this.skillRepository = skillRepository;
    }

    @PostConstruct
    public void init() {
        Skill java = new Skill();
        java.setSkillName("Java");
        java.setDesctiption("Java jest super");
        skillRepository.save(java);

        Skill cSharp = new Skill();
        cSharp.setSkillName("C#");
        cSharp.setDesctiption("C# jest wazny");
        skillRepository.save(cSharp);

        Developer developer1 = new Developer();
        developer1.setUsername("Dev1");
        developer1.setPassword("pass1");
        developer1.setFirstName("Luk");
        developer1.setLastName("Podolski");
        developer1.setImageUrl("https://toppng.com/uploads/preview/roger-berry-avatar-placeholder-11562991561rbrfzlng6h.png");
        developer1.setSkills(Arrays.asList(new Skill()));
        developer1.setEmail("test@g.pl");
        developerRepository.save(developer1);

        Developer developer2 = new Developer();
        developer2.setUsername("Dev2");
        developer2.setPassword("pass2");
        developer2.setFirstName("Rob");
        developer2.setLastName("Bob");
        developer2.setImageUrl("https://image.shutterstock.com/image-vector/gray-photo-placeholder-icon-design-260nw-1898064247.jpg");
        developer2.setSkills(Arrays.asList(new Skill()));
        developer2.setEmail("test2@g.pl");
        developerRepository.save(developer2);

        Developer developer3 = new Developer();
        developer3.setUsername("Dev3");
        developer3.setPassword("pass3");
        developer3.setFirstName("Ala");
        developer3.setLastName("Kot");
        developer3.setImageUrl("https://st3.depositphotos.com/9998432/13335/v/600/depositphotos_133351974-stock-illustration-default-placeholder-woman.jpg");
        developer3.setSkills(Arrays.asList(new Skill()));
        developer3.setEmail("test3@g.pl");
        developerRepository.save(developer3);


    }
}