package com.dieboldnixdorf.Jobportal.services;

import com.dieboldnixdorf.Jobportal.exception.UserNotFoundException;
import com.dieboldnixdorf.Jobportal.model.Developer;
import com.dieboldnixdorf.Jobportal.repository.DeveloperRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class DevelopersService {

    private final DeveloperRepository developerRepository;

    public List<Developer> getAllDevs() {
        return developerRepository.findAll();
    }

    public Developer getDevById(Long id) {
        return developerRepository.getDeveloperById(id).orElseThrow();
    }

    public Developer addDeveloper(Developer developer) {
        return developerRepository.save(developer);
    }

    public void deleteDeveloper(Long id) {
        developerRepository.deleteDeveloperById(id);
    }

    public Developer getDevByEmail(String email) {
        return developerRepository.getDeveloperByEmail(email).orElseThrow(() -> new UserNotFoundException("User with email: " + email + " now found"));
    }

    public Developer updateDeveloperById(Developer updatedDeveloper, Long id) {
        Developer developer = developerRepository.getDeveloperById(id)
                .orElseThrow(() -> new UserNotFoundException("User with id: " + id + " not found"));
        developer.setImageUrl(updatedDeveloper.getImageUrl());
        developer.setSkills(updatedDeveloper.getSkills());
        developer.setLastName(updatedDeveloper.getLastName());
        developer.setFirstName(updatedDeveloper.getFirstName());
        developer.setDescription(updatedDeveloper.getDescription());
        developer.setEmail(updatedDeveloper.getEmail());
        return developerRepository.save(developer);
    }
}
