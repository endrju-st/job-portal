package com.dieboldnixdorf.Jobportal.services;

import com.dieboldnixdorf.Jobportal.model.Employer;
import com.dieboldnixdorf.Jobportal.repository.EmployerRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class EmployerService {
    private final EmployerRepository employerRepository;


    public List<Employer> getAllEmployers() {
        return employerRepository.findAll();
    }

    public Employer getEmployerById(Long id) {
        return getEmployerById(id);
    }

    public Employer addEmployer(Employer employer) {
        return employerRepository.save(employer);
    }

    public void deleteEmployer(Long id) {
        employerRepository.deleteById(id);
    }
}
