package com.dieboldnixdorf.Jobportal.services;

import com.dieboldnixdorf.Jobportal.exception.JobOfferNotFoundException;
import com.dieboldnixdorf.Jobportal.model.JobOffer;
import com.dieboldnixdorf.Jobportal.repository.JobOfferRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class JobOfferService {

    private final JobOfferRepository jobOfferRepository;

    public List<JobOffer> getAllOffers() {
        return jobOfferRepository.findAll();
    }

    public JobOffer findOfferById(Long id) {
        return jobOfferRepository.getJobOfferById(id).orElseThrow();
    }

    public JobOffer addJobOffer(JobOffer jobOffer) {
        return jobOfferRepository.save(jobOffer);
    }

    public JobOffer updateJobOffer(JobOffer updatedJobOffer, Long id) {
        JobOffer jobOffer = jobOfferRepository.getJobOfferById(id).orElseThrow(() -> new JobOfferNotFoundException("Job offer with " + id + " not found"));
        jobOffer.setDescription(updatedJobOffer.getDescription());
        jobOffer.setActive(updatedJobOffer.isActive());
        jobOffer.setTitle(updatedJobOffer.getTitle());
        return jobOfferRepository.save(jobOffer);
    }

}
